'use strict';

const expect = require('chai').expect;

const properties_parser = require('../../lib/parsers/properties');

describe('lib/parsers/ini', function () {

  const test_data = `
db_user = dbuser
db_password = dbpassword
db_name = use_this_database

# This should also be ignored.
datadir = /var/lib/data
`;

  const expected_obj = {
    'db_user': 'dbuser',
    'db_password': 'dbpassword',
    'db_name': 'use_this_database',
    'datadir': '/var/lib/data',
  };

  describe('Public API', () => {
    it('Has callable parse method', () => {
      expect(properties_parser).to.respondTo('parse');
      expect(properties_parser.parse).to.be.a('function');
    });

    it('Read INI Data', () => {
      const actual_data = properties_parser.parse(test_data);
      expect(actual_data).to.deep.equal(expected_obj);
    });

    it('Undefined Data', () => {
      expect(properties_parser.parse).to.throw(
          'Cannot parse empty properties data.');
    });

    it('Null Data', () => {
      expect(properties_parser.parse.bind(null)).to.throw(
          'Cannot parse empty properties data.');
    });

    it('Invalid Data', () => {
      const invalidData = 'balls trololololol';
      expect(properties_parser.parse.bind(invalidData)).to.throw(
          'Cannot parse empty properties data.');
    });
  });

});
