'use strict';

const expect = require('chai').expect;

const yaml = require('yaml');
const yaml_parser = require('../../lib/parsers/yaml');

describe('lib/parsers/yaml', function () {

    const test_data = Object.freeze({
        "root": {
            "one": 1,
            "two": 2.2
        },
        "anotherRoot": "3.3",
        "arrayRoot": ['a', 'b', '3']
    });

    describe('Public API', () => {
        it('Has callable parse method', () => {
            expect(yaml_parser).to.respondTo('parse');
            expect(yaml_parser.parse).to.be.a('function');
        });

        it('Read YAML Data', () => {
            const yaml_data = yaml.stringify(test_data);
            const actual_data = yaml_parser.parse(yaml_data);
            expect(actual_data).to.deep.equal(test_data);
        });

        it('Undefined Data', () => {
            expect(yaml_parser.parse).to.throw('Cannot parse empty YAML data.');
        });

        it('Null Data', () => {
            expect(yaml_parser.parse.bind(null)).to.throw(
                'Cannot parse empty YAML data.');
        });

        it('Invalid Data', () => {
            const invalidData = 'balls trololololol';
            expect(yaml_parser.parse.bind(invalidData)).to.throw(
                'Cannot parse empty YAML data.');
        });
    });

});
