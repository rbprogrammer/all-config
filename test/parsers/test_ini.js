'use strict';

const expect = require('chai').expect;

const ini_parser = require('../../lib/parsers/ini');

describe('lib/parsers/ini', function () {

  const test_data = `
; this comment is being ignored
scope = global

[database]
user = dbuser
password = dbpassword
database = use_this_database

[paths.default]
datadir = /var/lib/data
array[] = first value
array[] = second value
array[] = third value
`;

  const expected_obj = {
    'scope': 'global',

    'database': {
      'user': 'dbuser',
      'password': 'dbpassword',
      'database': 'use_this_database'
    },

    'paths': {
      'default': {
        'datadir': '/var/lib/data',
        'array': ['first value', 'second value', 'third value']
      }
    }
  };

  describe('Public API', () => {
    it('Has callable parse method', () => {
      expect(ini_parser).to.respondTo('parse');
      expect(ini_parser.parse).to.be.a('function');
    });

    it('Read INI Data', () => {
      const actual_data = ini_parser.parse(test_data);
      expect(actual_data).to.deep.equal(expected_obj);
    });

    it('Undefined Data', () => {
      expect(ini_parser.parse).to.throw('Cannot parse empty INI data.');
    });

    it('Null Data', () => {
      expect(ini_parser.parse.bind(null)).to.throw(
          'Cannot parse empty INI data.');
    });

    it('Invalid Data', () => {
      const invalidData = 'balls trololololol';
      expect(ini_parser.parse.bind(invalidData)).to.throw(
          'Cannot parse empty INI data.');
    });
  });

});
