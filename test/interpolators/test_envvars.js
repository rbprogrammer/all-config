'use strict';

const expect = require('chai').expect;

const interpolator = require('../../lib/interpolators/envvars');

describe('lib/interpolators/envvars', function () {

  function check(toInterpolate, expected) {
    const actual = interpolator.interpolate(toInterpolate);
    if (expected === null) {
      expect(actual).to.be.null;
    } else {
      expect(actual).to.deep.equal(expected);
    }
  }

  describe('Public API', () => {

    beforeEach('Validating environment variables', function () {
      expect(process.env['USER']).to.not.be.null;
      expect(process.env['BOLLOCKS123']).to.be.undefined;
    });

    it('Has callable interpolate method', () => {
      expect(interpolator).to.respondTo('interpolate');
      expect(interpolator.interpolate).to.be.a('function');
    });

    it('Interpolate variables', () => {
      const user = process.env['USER'];
      check('${env:USER}', user);
      check('abc-${env:USER}', 'abc-' + user);
      check('${env:USER}-def', user + '-def');
      check('abc-${env:USER}-def', 'abc-' + user + '-def');
    });

    it('Interpolate variables with that do not exist', () => {
      const msg = 'Could not interpolate variable: BOLLOCKS123!';
      const expectThrows = (value) => {
        const badFunction = interpolator.interpolate.bind(interpolator, value);
        expect(badFunction).to.throw(msg);
      };
      expectThrows('${env:BOLLOCKS123}');
      expectThrows('abc-${env:BOLLOCKS123}');
      expectThrows('${env:BOLLOCKS123}-def');
      expectThrows('abc-${env:BOLLOCKS123}-def');
    });

    it('Interpolate variables with default values', () => {
      const defVal = 'defVal';
      check('${env:BOLLOCKS123:defVal}', defVal);
      check('abc-${env:BOLLOCKS123:defVal}', 'abc-' + defVal);
      check('${env:BOLLOCKS123:defVal}-def', defVal + '-def');
      check('abc-${env:BOLLOCKS123:defVal}-def', 'abc-' + defVal + '-def');
    });
  });

});
