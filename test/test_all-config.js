'use strict';

const chai = require('chai');
const expect = chai.expect;
const fs = require('fs');
const tmp = require('tmp');

const yacjs = require('../lib/all-config');

chai.use(require("chai-as-promised"));

describe('lib/yacjs', function () {

    const TEST_DATA = {
        "root": {
            "one": 1,
            "two": 2.2
        },
        "anotherRoot": "3.3"
    };

    let TEST_FILE_NAME = null;

    function setupTestFile(extension, data) {

        const normalize = (thing) =>
            (typeof thing !== 'undefined' && thing !== null) ? thing : '';
        extension = normalize(extension);
        data = normalize(data);

        const tempFileOpts = Object.freeze({
            discardDescriptor: true,
            prefix: 'test-yacjs-',
            postfix: extension
        });

        let tmpobj = tmp.fileSync(tempFileOpts);
        TEST_FILE_NAME = tmpobj.name;

        fs.writeFileSync(TEST_FILE_NAME, data);
    }

    afterEach(() => {
        if (TEST_FILE_NAME != null) {
            fs.unlinkSync(TEST_FILE_NAME);
        }
        TEST_FILE_NAME = null;
    });

    describe('Public API', () => {
        it('has callable parse method', () => {
            expect(yacjs).to.respondTo('read');
            expect(yacjs.read).to.be.a('function');
        });
    });

    describe('FileType Errors', () => {

        it('No File Extension', () => {
            setupTestFile();
            const errorMsg =
                `File type cannot be determined: ${TEST_FILE_NAME}`;
            const readData = yacjs.read(TEST_FILE_NAME);
            expect(readData).to.eventually.be.rejectedWith(errorMsg);
        });

        it('Unknown File Extension', async () => {
            const ext = 'unknownExtension';
            setupTestFile('.' + ext);
            const errorMsg = `Unknown file extension: ${ext}`;
            const readData = yacjs.read(TEST_FILE_NAME);
            expect(readData).to.eventually.be.rejectedWith(errorMsg);
        });
    });

    describe('INI', () => {
        beforeEach(() => {
            // TODO implement
            // setupTestFile('.json', JSON.stringify(TEST_DATA));
        });
        it('Read INI Data', async () => {
            throw new Error('Not implemented yet.');
        });
    });

    describe('JSON', () => {
        beforeEach(() => {
            setupTestFile('.json', JSON.stringify(TEST_DATA));
        });
        it('Read JSON Data', async () => {
            const readData = await yacjs.read(TEST_FILE_NAME);
            expect(readData).to.deep.equal(TEST_DATA);
        });
    });

    describe('PROPERTIES', () => {
        beforeEach(() => {
            // TODO implement
            // setupTestFile('.json', JSON.stringify(TEST_DATA));
        });
        it('Read PROPERTIES Data', async () => {
            throw new Error('Not implemented yet.');
        });
    });

    describe('YAML', () => {
        beforeEach(() => {
            // TODO implement
            // setupTestFile('.json', JSON.stringify(TEST_DATA));
        });
        it('Read YAML Data', async () => {
            throw new Error('Not implemented yet.');
        });
    });

});
