'use strict';

/**
 * Parses a JSON string into a object.
 *
 * @param data The JSON data to be converted into an object.
 * @returns {ReadonlyArray<any>} Frozen object from the supplied JSON data.
 */
module.exports.parse = (data) => {
    if (typeof data === 'undefined' || data === null) {
        throw 'Cannot parse empty JSON data.';
    }
    return Object.freeze(JSON.parse(data));
};
