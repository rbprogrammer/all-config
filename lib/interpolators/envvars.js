'use strict';

// TODO docs
// TODO unit test
function getEnvValue(toInterpolate) {
  // Remove the ${env:...} format.
  toInterpolate = toInterpolate.replace(/^\${env:/, '').replace(/}$/, '');

  // Split the remainder into variableName and defaultValue
  toInterpolate = toInterpolate.split(':');

  const variableName = String(toInterpolate[0]).trim();
  const defaultValue =
      toInterpolate[1] ? String(toInterpolate[1]).trim() : null;

  if (process.env.hasOwnProperty(variableName)) {
    return process.env[variableName];
  } else if (defaultValue !== null) {
    return defaultValue;
  } else {
    throw `Could not interpolate variable: ${toInterpolate}!`;
  }
}

// TODO docs
// TODO implement
// TODO unit test
module.exports.interpolate = (value) => {
  return value.replace(/\${env:[A-Za-z0-9:_.]+}/g, getEnvValue);
};
